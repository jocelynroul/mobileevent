package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.example.xavier.MobileEvent.model.Match;

import java.util.List;

/**
 * Created by x
 */
@Dao
public interface MatchDAO {
    @Insert
    void insertMatch(Match match);

    @Query("SELECT * FROM `Match` WHERE eventId=:id")
    List<Match> getMatchs(int id);
}
