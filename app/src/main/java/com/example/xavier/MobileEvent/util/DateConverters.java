package com.example.xavier.MobileEvent.util;

import android.arch.persistence.room.TypeConverter;

import java.util.Date;

/**
 * Created by x
 */

public class DateConverters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
}

