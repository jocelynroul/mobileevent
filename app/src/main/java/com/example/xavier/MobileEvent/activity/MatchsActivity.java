package com.example.xavier.MobileEvent.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;

import com.example.xavier.MobileEvent.dao.RoomDatabaseM2L;
import com.example.xavier.MobileEvent.model.Match;
import com.example.xavier.MobileEvent.R;
import com.example.xavier.MobileEvent.listAdapter.MatchListAdapter;

import java.util.List;
import java.util.concurrent.ExecutionException;

public class MatchsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matches);
        Bundle b = getIntent().getExtras();
        if (b!=null) {
            int codeEvt = b.getInt("codeEvt");
            try {
                List<Match> matchs = new RoomDatabaseM2L.selectMatchs(getApplicationContext()).execute(codeEvt).get();
                MatchListAdapter adapter = new MatchListAdapter(getApplicationContext(), matchs);
                ListView list = findViewById(R.id.lst_match);
                list.setAdapter(adapter);
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }
    }
}
