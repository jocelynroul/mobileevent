package com.example.xavier.MobileEvent.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

/**
 * Created by x
 */

@Entity
public class Event {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private String titre;

    public Event(@NonNull String titre){
        this.titre=titre;
    }

    @NonNull
    public String getTitre(){
        return this.titre;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
