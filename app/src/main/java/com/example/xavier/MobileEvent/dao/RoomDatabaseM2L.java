package com.example.xavier.MobileEvent.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;
import android.os.AsyncTask;

import com.example.xavier.MobileEvent.util.DateConverters;
import com.example.xavier.MobileEvent.model.Event;
import com.example.xavier.MobileEvent.model.Match;

import java.util.List;
/**
 * Created by x
 */
@Database(entities = {Event.class,Match.class},version = 1,exportSchema = false)
@TypeConverters(DateConverters.class)
public abstract class RoomDatabaseM2L extends RoomDatabase {
    public abstract EventDAO eventDAO();
    public abstract MatchDAO matchDAO();
    private static volatile RoomDatabaseM2L INSTANCE;

    static RoomDatabaseM2L getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (RoomDatabaseM2L.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),RoomDatabaseM2L.class, "M2LDatabase").build();
                }
            }
        }
        return INSTANCE;
    }

    public static class addEvent extends AsyncTask<Event,Void,Void>{
        private EventDAO eventDao;
        public addEvent(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao=db.eventDAO();
        }
        @Override
        protected Void doInBackground(Event... events) {
            eventDao.insertEvent(events[0]);
            return null;
        }
    }

    public static class selectEvents extends AsyncTask<Void, Void, List<Event>> {
        private EventDAO eventDao;
        public selectEvents(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao=db.eventDAO();
        }
        @Override
        protected List<Event> doInBackground(Void... voids) {
            List<Event> allEvents;
            allEvents = eventDao.getAllEvents();
            return allEvents;
        }
    }

    public static class selectEventById extends AsyncTask<Integer, Void, Event> {
        private EventDAO eventDao;
        public selectEventById(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao=db.eventDAO();
        }
        @Override
        protected Event doInBackground(Integer... integers) {
            Event event;
            event = eventDao.getEventById(integers[0]);
            return event;
        }
    }

    public static class delEvent extends AsyncTask<Integer, Void, Void> {
        private EventDAO eventDao;
        public delEvent(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao=db.eventDAO();
        }
        @Override
        protected Void doInBackground(Integer... integers) {
            eventDao.deleteEvent(integers[0]);
            return null;
        }
    }

    public static class updateEvent extends AsyncTask<String, Void, Void> {
        private EventDAO eventDao;
        public updateEvent(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            eventDao=db.eventDAO();
        }
        @Override
        protected Void doInBackground(String... strings) {
            eventDao.updateEvent(Integer.parseInt(strings[0]),strings[1]);
            return null;
        }
    }

    public static class addMatch extends AsyncTask<Match,Void,Void>{
        private MatchDAO matchDao;
        public addMatch(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDao=db.matchDAO();
        }
        @Override
        protected Void doInBackground(Match... matches) {
            matchDao.insertMatch(matches[0]);
            return null;
        }
    }

    public static class selectMatchs extends AsyncTask<Integer, Void, List<Match>> {
        private MatchDAO matchDao;
        public selectMatchs(Context context){
            RoomDatabaseM2L db = RoomDatabaseM2L.getDatabase(context);
            matchDao=db.matchDAO();
        }
        @Override
        protected List<Match> doInBackground(Integer... integers) {
            List<Match> matchs;
            matchs = matchDao.getMatchs(integers[0]);
            return matchs;
        }
    }
}
