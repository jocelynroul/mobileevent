package com.example.xavier.MobileEvent.model;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

/**
 * Created by x
 */

@Entity
public class Match {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @NonNull
    private Date dateRencontre;
    @NonNull
    private String titre;
    @ForeignKey(entity = Event.class, parentColumns = "id", childColumns = "eventId")
    private int eventId;

    public int getEventId() {
        return eventId;
    }

    public Match(@NonNull String titre, @NonNull Date dateRencontre, int eventId) {
        this.titre=titre;
        this.dateRencontre=dateRencontre;
        this.eventId = eventId;
    }

    @NonNull
    public Date getDateRencontre() {
        return dateRencontre;
    }

    @NonNull
    public String getTitre() {
        return titre;
    }

    public void setTitre(@NonNull String titre) {
        this.titre = titre;
    }

    public int getId(){
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
